DEVELOPER INFO
==============

CODING CONVENTIONS
------------------

We are aware of the fact that not the whole codebase uses the same conventions at the moment.
But we would like to establish the following rules, based on best practices. The numbering has no deeper meaning,
its just for referencing the items.

1. Use the tabs for indentation. Set the tabwidth of your editor to 4.

2. Allow extra space within parentheses, like this:

     while( !done ) {
             foo();
     }

3. Please don't refactor our code because you just don't like its style or you think that things could be
just a little bit better if reformat the code to fit your style.

4. Use curly braces for all if statements, even one liners. We don't need to minimize the lines of code.

Good: if( a ){
        doB();
      }

Error prone: if( a ) doB();

     if( a )
        doB();

5. Method names follow the camel case naming scheme, starting with a lowercase letter.

  Example: void doB( int * myArgument );

6. Use speaking and self-explaining names for your variables (exception: loop-variables). We don't need to use short
likes i,n,aux etc...

7. Prepend pointer types with a "p" (for example: pMySample), floats with an "f", integer types with an "n" and 
   members of a class with an "m_" (for example: m_pEngine)

8. Use "auto" or range-based for loops to make iterations on container classes more readable

9. The singleton pattern is quite over-used in Hydrogen. If you're adding new classes / servers, 
   please take a moment to consider if it really has to be a singleton.

10. Maximum line length is 120 chars

11. Hydrogen is a cross platform application, which needs to be build on a great range of architectures and operating systems.
    Use only C++17 features and do not use any platform specific compiler extensions without the need to do so.

COMMITTING YOUR CODE
-------------------

The easiest way to participate in the development of hydrogen is to create a fork at github and create a pull request
from your changes.

Please take the following things into account:

1. If you want to send us a bug fix, please include only the commits which are part of the bug fix. Do *not* mix in new
features or refactor code.

2. You can reference the github issue number in your pull request if you want to fix a bug which is already known in our bugtracker


MAKING A RELEASE
----------------

Transitioning the code to remove some of the development hooks in
order to make a release has several, easy-to-forget steps.  They are:

  1. Set the version
     a. Configure the correct version in CMakeLists.txt

     b. Update ChangeLog

     c. Check if nothing has changed and update version and date in
        linux/hydrogen.1

     d. Update the TARGET_VERSION variable in .appveyor.yml

  2. Run `src/gui/src/about_dialog_contributor_list_update.sh
     GIT_TAG_OF_LAST_RELEASE HEAD` to update the list of recent
     contributors.

  3. Add a corresponding version tag in the documentation and update
     the submodule.

  4. Commit your changes.

  5. Create a dedicated branch containing the suffix `-artifacts`,
     like `releases/1.2.2-release-artifacts`, and manually set in
     `CMakeLists.txt` the `IS_DEVEL_BUILD` variable to "false" and
     ensure `DISPLAY_VERSION` does not have a suffix. When pushing
     this branch to Github, our AppVeyor pipeline will create release
     artifacts for Linux, macOS, and Windows.

  6. Call your friends.  Have a party.  Be sure to tests the artifacts
     on as many systems as possible.  Be sure to install and uninstall
     them, too.

  7. If the release passes these "internal" tests, add new version,
     date, ChangeLog as well as the release artifacts including their
     SHA256 sums to `linux/org.hydrogenmusic.Hydrogen.metainfo.xml.in`
     and run `make` in `linux/` folder

  8. Tag the release.  Remember, after tagging the release you may not
     commit changes to the tag.

     git tag -a 0.9.4 -m "Tagging 0.9.4"
     git push --tags origin

  9. In case the release is a new major or minor version, also create
     a dedicated release branch, like `releases/1.2`.

  10. Create a github release, incorporating binary packages, source
      package, tag and release announcement.

  11. Upload the release to SourceForge and make it the default download
      for all supported platforms.

  12. Make release announcements on
      - hydrogen-users@lists.sourceforge.net
      - hydrogen-music.org

  13. Update third party repos. Make a PR against:
      - Homebrew: https://github.com/Homebrew/homebrew-cask/blob/master/Casks/hydrogen.rb
      - Flatpak: https://github.com/flathub/org.hydrogenmusic.Hydrogen/
